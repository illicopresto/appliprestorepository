package fr.presto.servlet;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.presto.models.AlerteStock;
import fr.presto.models.PasDAlerteStock;

public class AlerteStockServletTest {
List<PasDAlerteStock> listeAlerte = new ArrayList<>();
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void ListeAlerteVidetest() {
		PasDAlerteStock alert = new PasDAlerteStock();
		listeAlerte.add(alert);
		for(PasDAlerteStock alerte : listeAlerte)
		{
			String test = alerte.getMessage();
			assertEquals("VOUS N'AVEZ AUCUNE ALERTE DE STOCK, VOUS ETES UN RESTAURATEUR DE GENIE!!!", test);
		}
	}

	@Test
	public void ListeAlertetest() {
		AlerteStock alert = new AlerteStock(1, "Tomates", 5);
		listeAlerte.add(alert);
		for(PasDAlerteStock alerte : listeAlerte)
		{
			String test = alerte.getMessage();
			assertEquals("ATTENTION, IL NE RESTE PLUS QUE 5 UNITE DE Tomates BANDE DE BUSES!!", test);
		}
	}
}
