package fr.presto.DAO;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


import fr.presto.models.Plats;

@Stateless
public class PlatsDAO {
	
	@PersistenceContext(name = "Presto")
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	public List<Plats> getList() {
		Query query = em.createQuery("from plats");
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getListNomPlat(){
		Query query = em.createQuery("select distinct nom from plats");
		return (List<String>) query.getResultList();
	}


	public void add(Plats plat) {
		em.persist(plat);
	}

	public Plats get(int id) {
		return em.find(Plats.class, id);
	}

	public void remove(Plats plat) {
		plat = em.merge(plat);
		em.remove(plat);
	}

	public void update(Plats plat) {
		
		em.merge(plat);

	}

	
}
