package fr.presto.DAO;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import fr.presto.models.Stock;

@Stateless
public class StockDao {
	
	@PersistenceContext(name = "Presto")
	private EntityManager em;

	@SuppressWarnings("unchecked")
	public List<Stock> getListe() {
		Query query = em.createQuery("from stock");
		return query.getResultList();

	}
	

	public void add(Stock stock) {

		em.persist(stock);

	}

	public Stock get(int id) {

		return em.find(Stock.class, id);

	}

	public void remove(Stock stock) {
		stock = em.merge(stock);   //permet d'avoir un objet attaché que entityManager peut effacer
		em.remove(stock);

	}

	public void update(Stock stock) {

		em.merge(stock);

	}
}