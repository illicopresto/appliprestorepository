package fr.presto.models;



import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="plats")
public class Plats {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id; 
	private String nom; 
	private String ingredient; 
	private int nbIngredient; 
	
	
	public Plats(){
		
	}

	public int getId() {
		return id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getIngredient() {
		return ingredient;
	}

	public void setIngredient(String ingredient) {
		this.ingredient = ingredient;
	}

	public int getNbIngredient() {
		return nbIngredient;
	}

	public void setNbIngredient(int nbIngredient) {
		this.nbIngredient = nbIngredient;
	}






	
}
