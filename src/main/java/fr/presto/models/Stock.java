package fr.presto.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name ="stock")
public class Stock {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String nom;
	private int quantite;
	private int seuil;

	public int getId() {
		return id;
	}

	public String getNom() {
		return nom;
	}

	public int getQuantite() {
		return quantite;
	}

	public int getSeuil() {
		return seuil;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}

	public void setSeuil(int seuil) {
		this.seuil = seuil;
	}

}
