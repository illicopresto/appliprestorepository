package fr.presto.models;




public class AlerteStock extends PasDAlerteStock {

	private int id;

	private String nom;
	private int quantite;
	private int seuil;
	private String message;

	
	
	
	public AlerteStock(int id, String nom, int quantite) {
		this.id = id;
		this.nom = nom;
		this.quantite = quantite;
		this.message = ("ATTENTION, IL NE RESTE PLUS QUE " + quantite + " UNITE DE " + nom + " BANDE DE BUSES!!");
	}

	public int getId() {
		return id;
	}

	public String getNom() {
		return nom;
	}
	
	public String getMessage() {
		return message;
	}

	public int getQuantite() {
		return quantite;
	}

	public int getSeuil() {
		return seuil;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}

	public void setSeuil(int seuil) {
		this.seuil = seuil;
	}

}
