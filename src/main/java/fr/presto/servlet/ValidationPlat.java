package fr.presto.servlet;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.presto.DAO.PlatsDAO;
import fr.presto.models.Plats;

/**
 * Servlet implementation class ValidationPlat
 */
@SuppressWarnings("serial")
public class ValidationPlat extends HttpServlet {
	@EJB
	PlatsDAO platDao;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nomPlat= (String) request.getSession().getAttribute("NomPlat"); 
		String choix = (String) request.getParameter("Validation");	 
		String nomIngredient = (String) request.getParameter("Ingredient"); 
		int nombre = (int) Integer.parseInt(request.getParameter("nombre")); 
		if("1".equals(choix)){
			
		}else{
			Plats plat = new Plats(); 
			plat.setNom(nomPlat);
			plat.setIngredient(nomIngredient);
			plat.setNbIngredient(nombre);
			platDao.add(plat);
			
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/AjoutIngredient.jsp"); 
			dispatcher.forward(request, response);
		}
	}

}
