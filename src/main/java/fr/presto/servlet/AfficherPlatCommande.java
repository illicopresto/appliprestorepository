package fr.presto.servlet;
import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.presto.DAO.PlatsDAO;
import fr.presto.models.Plats;

/**
 * Servlet implementation class afficherPlatCommande
 */
public class AfficherPlatCommande extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
	@EJB
	PlatsDAO platDao;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List <String> nomPlats = platDao.getListNomPlat();
		request.setAttribute("nomPlats", nomPlats); 
		
		//mettre action plat plus dispo

		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher( "/WEB-INF/affichagePlatCommande.jsp");
		
		dispatcher.forward( request, response );
	}


}