package fr.presto.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.presto.models.Stock;



public class Rendu
{
	public static void pageLogin( ServletContext context, HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException
	{
		pagePrincipale( "Bienvenue", "/WEB-INF/login.jsp", context, request, response );
	}
	
	
	public static void pageBienvenue( ServletContext context, HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException
	{

		pagePrincipale( "Bienvenue", "/WEB-INF/accueil.jsp", context, request, response );
	}

	public static void pageStock( List<Stock> stocks, ServletContext context, HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException
	{
		request.setAttribute( "stocks", stocks );
		//request.setAttribute( "permission", permission );
		//Attention à la permission!!!
	
		pagePrincipale( "Stock", "/WEB-INF/stock.jsp", context, request, response );

	}
	
	public static void pageAjouterStock(String message, ServletContext context, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		
		request.setAttribute("message", message);
		
		pagePrincipale("Ajouter Ingrédients", "/WEB-INF/ajouterStock.jsp", context, request, response);
		
	}
//	
//	public static void pagePlat (String titrePage, List<Plat> plats, boolean montrerListStock, boolean montrerListPlat, ServletContext context, HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException
//	{
//		request.setAttribute("plat", plats);
//		request.setAttribute( "montrerListStock", montrerListStock );
//		request.setAttribute( "montrerActionsCarte", montrerListPlat );
//		
//		pagePlat( titrePage, "/WEB-INF/plat.jsp", context, request, response);
//		
//	}
//
	public static void pagePrincipale( String title, String contentJsp, ServletContext context, HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException
	{
		request.setAttribute( "pageTitle", title );
		request.setAttribute( "contentJsp", contentJsp );

		RequestDispatcher dispatcher = context.getRequestDispatcher( contentJsp );
		
		dispatcher.forward( request, response );
	}
}
