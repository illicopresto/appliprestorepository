package fr.presto.servlet;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.presto.DAO.StockDao;
import fr.presto.models.Stock;

public class ModifierStockServlet extends HttpServlet implements Serializable {

	private static final long serialVersionUID = 1L;
	@EJB

	private StockDao stockDao;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		proceed(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		proceed(request, response);
	}

	private void proceed(HttpServletRequest request, HttpServletResponse response) throws IOException {
		try {
			int id = Integer.parseInt(request.getParameter("id"));
			int quantite = Integer.parseInt(request.getParameter("quantiteModifier"));

			List<Stock> stocks = stockDao.getListe();
			Stock stock = new Stock();
			for (Stock iStock : stocks) {
				if (iStock.getId() == id) {
					stock = iStock;
				}
			}
			int newQuantity = stock.getQuantite() + quantite;
			stock.setQuantite(newQuantity);
			stockDao.update(stock);

		} catch (Exception e) {
			System.out.println("\n\n !!!! Erreur méthode proceed dans ModifierStockServlet !!!!!! \n\n");
		}
		response.sendRedirect("stock");
	}
}
