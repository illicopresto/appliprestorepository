package fr.presto.servlet;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.presto.DAO.StockDao;
import fr.presto.models.Stock;

public class SupprimerStockServlet extends HttpServlet implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private StockDao stockDao;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int requestStockId = Integer.parseInt(request.getParameter("id"));
		removeStock(requestStockId);
		response.sendRedirect("stock");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	public void removeStock(int requestStockId) {
		try {
			List<Stock> stocks = stockDao.getListe();
			Stock stock = new Stock();
			for (Stock iStock : stocks) {
				if (iStock.getId() == requestStockId) {
					stock = iStock;
				}
			}
			stockDao.remove(stock);
		} catch (Exception e) {
			System.out.println("\n\n !!!! Erreur méthode proceed dans SupprimerStockServlet !!!!!! \n\n");
		}
	}
}
