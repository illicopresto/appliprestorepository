package fr.presto.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.presto.DAO.PlatsDAO;
import fr.presto.models.Plats;


public class AfficherPlatServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@EJB
	PlatsDAO platDao;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<Plats> plats = platDao.getList();
		request.setAttribute("ListePlat", plats);
		request.setAttribute("plats", plats);

		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher( "/WEB-INF/AfficherPlat.jsp");
		
		dispatcher.forward( request, response );
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
