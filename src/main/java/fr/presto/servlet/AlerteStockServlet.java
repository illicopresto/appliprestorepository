package fr.presto.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.presto.servlet.Rendu;
import fr.presto.DAO.StockDao;
import fr.presto.models.AlerteStock;
import fr.presto.models.PasDAlerteStock;
import fr.presto.models.Stock;

public class AlerteStockServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
	@EJB
	private StockDao stockdao;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	process(req, resp, getServletContext());
	}
	


	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		process(req, resp, getServletContext());
	}

	
	private void process(HttpServletRequest req, HttpServletResponse resp, ServletContext servletContext) throws ServletException, IOException {
		List<Stock> monStock = stockdao.getListe();
		
		List<PasDAlerteStock> listeAlerte = new ArrayList<>();
		for(Stock stock : monStock)
		{
			if(stock.getQuantite()<=stock.getSeuil())
			{
				AlerteStock alert = new AlerteStock(stock.getId(), stock.getNom(), stock.getQuantite());
				listeAlerte.add(alert);
			}
			
		}
		
		if(listeAlerte.isEmpty())
		{
			PasDAlerteStock alert = new PasDAlerteStock();
			listeAlerte.add(alert);
			
		}
		
		req.setAttribute( "alertes", listeAlerte );
		Rendu.pageBienvenue(getServletContext(), req, resp);
//		resp.sendRedirect("home");
	}
}
