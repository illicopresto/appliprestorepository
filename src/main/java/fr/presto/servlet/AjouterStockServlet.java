package fr.presto.servlet;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
//import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.presto.DAO.StockDao;
import fr.presto.models.Stock;

//@ManagedBean
//@RequestScoped
public class AjouterStockServlet extends HttpServlet implements Serializable {

	@EJB
	private StockDao stockDao;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Rendu.pageAjouterStock("Veuillez compléter les champs suivants :", getServletContext(), request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String sNom = request.getParameter("nom");
		String sQuantite = request.getParameter("quantite");
		String sSeuil = request.getParameter("seuil");

		entreesFormulaireCorrectes(sNom, sQuantite, sSeuil, request, response);

		int iQuantite = Integer.parseInt(sQuantite);
		int iSeuil = Integer.parseInt(sSeuil);
		
		ajouterStockDansStocks(sNom, iQuantite, iSeuil);

		List<Stock> stocks = stockDao.getListe();
		Rendu.pageStock(stocks, getServletContext(), request, response);

	}

	private void entreesFormulaireCorrectes(String sNom, String sQuantite, String sSeuil, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		int iQuantite;
		int iSeuil;

		if (sNom == null || sNom.isEmpty()) {
			Rendu.pageAjouterStock("Le champs nom est vide", getServletContext(), request, response);
		}

		if (sQuantite == null || sQuantite.isEmpty()) {
			Rendu.pageAjouterStock("Le quantite nom est vide", getServletContext(), request, response);
		}

		if (sSeuil == null || sSeuil.isEmpty()) {
			Rendu.pageAjouterStock("Le seuil nom est vide", getServletContext(), request, response);
		}

		try {
			iQuantite = Integer.parseInt(sQuantite);
		} catch (Exception e) {
			Rendu.pageAjouterStock("La quantite est incorrecte", getServletContext(), request, response);
		}

		try {
			iSeuil = Integer.parseInt(sSeuil);
		} catch (Exception e) {
			Rendu.pageAjouterStock("Le seuil est incorrect", getServletContext(), request, response);
		}
	}

	private void ajouterStockDansStocks(String sNom, int iQuantite, int iSeuil) {
		Stock stock = new Stock();
		stock.setNom(sNom);
		stock.setQuantite(iQuantite);
		stock.setSeuil(iSeuil);
		stockDao.add(stock);
	}
}
