package fr.presto.servlet;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.presto.DAO.PlatsDAO;
import fr.presto.models.Plats;

/**
 * Servlet implementation class ServletPlat
 */
public class ServletPlat extends HttpServlet {

	
	@EJB
	PlatsDAO platDao;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/CreerPlat.jsp"); 
		dispatcher.forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String nom = (String) request.getParameter("Nom");
		String choix = (String) request.getParameter("Validation");
		String nomIngredient = (String) request.getParameter("Ingredient");
		int nombre = (int) Integer.parseInt(request.getParameter("nombre"));
		if ("1".equals(choix)) {
			List<Plats> plats = platDao.getList();
			Rendu.pageBienvenue(getServletContext(), request, response); 
		} else {
			Plats plat = new Plats();
			plat.setNom(nom);
			plat.setIngredient(nomIngredient);
			plat.setNbIngredient(nombre);
			platDao.add(plat);

			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/CreerPlat.jsp");
			dispatcher.forward(request, response);
		}
	

	}

}
