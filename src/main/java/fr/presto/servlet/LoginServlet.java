package fr.presto.servlet;

import java.io.IOException;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.validator.ValidatorException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.presto.DAO.UtilisateurDao;

public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Rendu.pageLogin(getServletContext(), req, resp);
	}

	@EJB
	private UtilisateurDao utilisateurDao;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html");

		String login = request.getParameter("log");
		String mdp = request.getParameter("mdp");
		String message = "Merci de vous logger";

		
		System.out.println("login " + login);
		System.out.println("mdp " + mdp);
		
		if (login != null && !login.isEmpty() && mdp != null && !mdp.isEmpty()) {
			
//			if(login.equals("Admin"))
//				request.getRequestDispatcher("/WEB-INF/table.jsp").forward(request, response);
				

//			if(login.equals("Admin"))
//					Rendu.pageBienvenue(login,"/WEB-INF/login.jsp",true,getServletContext(), request, response);
//			
//			if(login.equals("Serveur"))
//				Rendu.pageBienvenue(login,"/WEB-INF/login.jsp",false,getServletContext(), request, response);
			
			if (!login.equals("Serveur") && !login.equals("Admin")) {
				message = "login incorrect";
				request.setAttribute("message", message);
				request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);

			}
			
			if (login.equals("Serveur") && !mdp.equals("serveur")) {
				request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
				message = "Mot de passe incorrect";
				request.setAttribute("message", message);
			}

			if (login.equals("Admin") && !mdp.equals("admin")) {
				message = "Mot de passe incorrect";
				request.setAttribute("message", message);
				request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
			}

			request.getSession().setAttribute("log", login);
			request.getSession().setAttribute("mdp", mdp);
//			Rendu.pageBienvenue(getServletContext(), request, response);
			response.sendRedirect("home");
		}
		
		
		else {
			message = "Merci de vous logger";
			request.setAttribute("message", message);
			request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
		}

	}
}
