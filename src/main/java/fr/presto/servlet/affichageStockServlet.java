package fr.presto.servlet;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.presto.DAO.StockDao;
import fr.presto.models.Stock;

public class affichageStockServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
   @EJB
   private StockDao stockDao;
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<Stock> stocks = stockDao.getListe();
		Rendu.pageStock(stocks, getServletContext(), request, response);
	}

}
