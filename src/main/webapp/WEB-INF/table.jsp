<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Table</title>

<!--Import Google Icon Font-->
<link href="http://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">

<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="css/materialize.min.css"
	media="screen,projection" />

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<style>
#dorpdown {
	position: absolute;
	top: 0%;
}
</style>

</head>

<body>
	<table>

		<td>Table</td>

		<c:forEach items="${requestScope['Tables']}" var="table">
			<tr>

				<td>${table.numero}</td>
				<td><a href="?action=effacer&id=${table.id}">
						<button>Effacer</button>
				</a></td>
			</tr>
		</c:forEach>
	</table>

	<a href="?action=ajouter">
		<button>Ajouter</button>
	</a>

	<%=request.getAttribute("message")%>

	<!--Import jQuery before materialize.js-->
	<script type="text/javascript"
		src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script type="text/javascript" src="js/materialize.min.js"></script>
</body>

</body>
</html>