<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="fr.presto.models.Utilisateur" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Presto</title>


<!--Import Google Icon Font-->
<link href="http://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">

<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="css/materialize.min.css"
	media="screen,projection" />

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<style>
body {
	text-align: center;
	background-position: center center;
	height: 835px;
}

#login {
	position: absolute;
	width: 30%;
	left: 35%;
	bottom: 30%;
}

#entrer {
	text-align: center;
	font-size: 24px;
	color: black;
	font-family: cursive;
}
</style>


</head>

<body>
<% Utilisateur utilisateur = (Utilisateur) request.getAttribute("utilisateur");
%>
	<br />

	<div id="login">
		<form method="post" action="login">
			<p id="entrer" style="">
			<h3>
				Bienvenue sur <b>Presto</b>
			</h3>
			<br /> <br /> <b>Veuillez entrez votre identifiant et votre mot de passe</b>
			</p>
			
			<br /> <br />
			<div class="input-field col s6">
				<input name="log" id="icon_prefix" type="text" class="validate" value="${utilisateur.log}"> 
				<label for="log"> Enter votre login</label>
			</div>
			<br />
			
			<div class="input-field">
				<i class="material-icons prefix">lock</i> <input id="password" type="password" class="validate" name="mdp" value="${utilisateur.mdp}"> 
				<label for="mdp">Entrer votre mot de passe </label>
			</div>

			<button class="btn waves-effect waves-light" type="submit">
				<i class="material-icons right">send</i>OK
			</button>

		</form>
	</div>


	<!--Import jQuery before materialize.js-->
	<script type="text/javascript"
		src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script type="text/javascript" src="js/materialize.min.js"></script>
</body>
</html>