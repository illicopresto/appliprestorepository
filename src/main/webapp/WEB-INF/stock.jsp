<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@page import="fr.presto.models.Stock"%>
<%@page import="java.util.List"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>



<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!--Import Google Icon Font-->
<link href="http://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">

<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="css/materialize.min.css"
	media="screen,projection" />

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />


</head>
<body>

	<nav>
	<div class="nav-wrapper">
		<a href="#!" class="brand-logo"></a>
		<ul class="right hide-on-med-and-down">
			<li><a href="ajouterStock" class="waves-effect waves-light btn">AJOUTER
					UN INGREDIENT</a></li>
		</ul>
	</div>
	</nav>

	<table class="highlight">
		<thead>
			<tr>
				<th data-field="nom">ID</th>
				<th data-field="nom">Nom</th>
				<th data-field="quantite">Quantité en stock</th>
				<th data-field="seuil">Seuil d'alerte</th>
				<th data-field="editStock">Editer le stock</th>
			</tr>
		</thead>

		<tbody>
			<c:forEach items="${requestScope['stocks']}" var="stock">
				<c:if test="${stock.seuil} < ${stock.quantite}">
				</c:if>
				<tr>
					<td>${stock.id}</td>
					<td>${stock.nom}</td>
					<td>${stock.quantite}</td>
					<td>${stock.seuil}</td>
					<td>
						<div class="row">

							<div class="col s2 m2 l2">
								<a href="supprimerStock?id=${stock.id}"
									class="btn-floating btn-large waves-effect waves-light red">
									<i class="material-icons">delete</i>
								</a>
							</div>
							<div class="col s10 m10 l10">
								<form method="post" action="modifierStock?id=${stock.id}">
									<div class="input-field">
										<i class="material-icons prefix">replay</i> <input id="name"
											type="text" class="validate" name="quantiteModifier">
										<label for="name">Variation du stock</label>
									</div>

									<!-- 							<button class="btn waves-effect waves-light" type="submit"> -->
									<!-- 								Valider <i class="material-icons right">send</i> -->
									<!-- 							</button> -->
								</form>
							</div>

						</div>
					</td>
				</tr>

			</c:forEach>
		</tbody>
	</table>

	<!--Import jQuery before materialize.js-->
	<script type="text/javascript"
		src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script type="text/javascript" src="js/materialize.min.js"></script>


</body>
</html>