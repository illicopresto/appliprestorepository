<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bienvenue</title>

<!--Import Google Icon Font-->
<link href="http://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">

<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="css/materialize.min.css"
	media="screen,projection" />

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />




<style>
body {
	text-align: center;
}

#acceuil {
	height: 800px;
	width: 800px;
	left: 27%;
	position: absolute;
}

#dorpdown {
	position: absolute;
	top: 0%;
}
</style>

<%
	String login = (String) request.getAttribute("login");
%>
</head>
<body>

	<%@ include file="naviguation.jsp"%>


	<div id="acceuil">
		<h1>Bienvenue!</h1>


		<br /> <br />
		<c:forEach items="${requestScope['alertes']}" var="alerte">
			<span> ${alerte.message} <br /> <br />

			</span>
		</c:forEach>
	</div>
	<!--Import jQuery before materialize.js-->
	<script type="text/javascript"
		src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script type="text/javascript" src="js/materialize.min.js"></script>


</body>
</html>