<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!--Import Google Icon Font-->
<link href="http://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">

<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="css/materialize.min.css"
	media="screen,projection" />

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />


</head>
<body style="margin-left: 2em;">
	<h1>Ajouter un produit</h1>

	<p><%=request.getAttribute("message")%></p>

	<form method="post">

		<div>
			<input id="nom" type="text" name="nom"> <label for="nom">Nom</label>
		</div>

		<div>
			<input id="quantite" type="text" name="quantite"> <label
				for="quantite">La quantité initiale</label>
		</div>

		<div>
			<input id="seuil" type="text" name="seuil"> <label
				for="seuil">Le seuil d'alerte</label>
		</div>

		<p><div>
			<input type="submit" name="envoyer" value="Valider">
		</div></p>


	</form>

	<!--Import jQuery before materialize.js-->
	<script type="text/javascript"
		src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script type="text/javascript" src="js/materialize.min.js"></script>

</body>
</html>
