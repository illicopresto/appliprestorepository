<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="fr.presto.models.Plats"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Plats</title>
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="css/materialize.min.css"
	media="screen,projection" />

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<style>
#dorpdown {
	position: absolute;
	top: 0%;
}
</style>
</head>
<body>

	<%@ include file="naviguation.jsp" %>
	
	<br />
	<br />
	<table class="highlight">
		<thead>
				<tr>
					<th data-fiel="nom">Nom</th>
					<th data-fiel="ingredient">Ingrédients</th>
					<th data-fiel="nbIngredient">Quantité</th>
				</tr>
			</thead>
				<c:forEach items="${requestScope['plats']}" var="plat">
					<tr>
						<td>${plat.nom}</td>
						<td>${plat.ingredient}</td>
						<td>${plat.nbIngredient}</td>
					</tr>
				</c:forEach>
				<c:forEach items="${requestScope['nomPlats']}" var="nomPlat">
					<tr>
						<td>${nomPlat}</td>
					</tr>
				</c:forEach>
	</table>
	
			<!--Import jQuery before materialize.js-->
		<script type="text/javascript"
			src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
		<script type="text/javascript" src="js/materialize.min.js"></script>
	

</body>
</html>