<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Affichage stock</title>
</head>
<body>
<div>
	<table>
		<thead>
			<tr>
				<th>Nom</th>
				<th>Quantité</th>
				<th>Seuil</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${requestScope['stock']}" var="ingredient">
				<tr>
					<td>${ingredient.nom}</td>
					<td>${ingredient.quantite}</td>
					<td>${ingredient.seuil}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>

</body>
</html>